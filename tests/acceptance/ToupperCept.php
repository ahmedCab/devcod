<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('ensure Toupper form works');
$I->amOnPage('/site/
	toupper.html');
$I->see('Convert Me!');
$I->fillField('string', "Convert me to upper");
$I->click('Convert');
$I->amOnPage('toupper.php');
$I->see('To Upper!');
